<?php
session_start();
?>
<!DOCTYPE html>
<html>
	<head>
		<!--Boostrap-->
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" />
		<!-- Font Awesome -->
		<script src="https://kit.fontawesome.com/ea33b306b1.js" crossorigin="anonymous"></script>
		<!--CSS-->
		<link rel="stylesheet" href="style.css" />
		<title>Toko Mobil Bekas</title>
	</head>
	<body id="bodi">
		<!-- Utama -->
		<section id="utama">
			<div class="container-fluid py-2">
				<div class="row py-2">
					<div class="col-md-12">
						<div id="ber-luar" class="row">
							<div id="ber" class="row">
								<div class="col-md-12 my-2">
									<hr id="baris">
									<h3 class="text-center">
										Toko Mobil Bekas
									</h3>
									<h6 class="text-center" style="margin-bottom: 0;">Keranjang Toko</h6>
									<hr id="baris">
								</div>
								<!-- Tabel -->
								<?php
									if (!empty($_SESSION['cart'])){										
										$max=sizeof($_SESSION['cart']['arrCart']);
								?>								
									<table class="table table-hover table-bordered" style="border: 2px;">
										<thead>
											<tr style="text-align:center; background-color:#d6d3d3;">
												<th scope="col">No</th>
												<th scope="col">Item</th>
												<th scope="col">Jumlah</th>
												<th scope="col">Harga</th>
												<th scope="col">Total Harga</th>
											</tr>
										</thead>
										<?php
										$sum = 0;
											for ($i=0;$i<$max;$i++){
												echo " <td align='center'>".($i+1)."</td>"; 
												foreach ($_SESSION['cart']['arrCart'][$i] as $key => $val){
												echo "<td align='center'>$val</td>"; 
												}												
												echo "<td align='center'>Rp.".$_SESSION['cart']['arrCart'][$i]['jml']*$_SESSION['cart']['arrCart'][$i]['hrg']."</td>";
												echo "</tr>";
												$sum += $_SESSION['cart']['arrCart'][$i]['jml']*$_SESSION['cart']['arrCart'][$i]['hrg'];
											}
											echo "	<tr>
														<td colspan=4 align='right'><b>Total Harga</b></td>
														<td align='center'>Rp.".$sum."</td>
													</tr>";
									?>
									</table>
								<?php
									echo"
									<div class='d-grid gap-2 d-flex justify-content-between'>
										<a href='utama.php'>
											<div class='d-grid gap-2 col-6'>
												<button id='tombol' class='btn' type='button' style='margin-bottom: 10px;'>
													<svg xmlns='http://www.w3.org/2000/svg' width='25' height='25' fill='currentColor' class='bi bi-arrow-left' viewBox='0 0 16 16'>
														<path fill-rule='evenodd' d='M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z'/>
													</svg>
												</button>
											</div>
										</a>
										<a href='kosong.php'>
											<div class='d-grid gap-2 col-6'>
												<button id='tombol' class='btn' type='button' style='margin-bottom: 10px;'>
													<svg xmlns='http://www.w3.org/2000/svg' width='25' height='25' fill='currentColor' class='bi bi-bag-x' viewBox='0 0 16 16'>
														<path fill-rule='evenodd' d='M6.146 8.146a.5.5 0 0 1 .708 0L8 9.293l1.146-1.147a.5.5 0 1 1 .708.708L8.707 10l1.147 1.146a.5.5 0 0 1-.708.708L8 10.707l-1.146 1.147a.5.5 0 0 1-.708-.708L7.293 10 6.146 8.854a.5.5 0 0 1 0-.708z'/>
														<path d='M8 1a2.5 2.5 0 0 1 2.5 2.5V4h-5v-.5A2.5 2.5 0 0 1 8 1zm3.5 3v-.5a3.5 3.5 0 1 0-7 0V4H1v10a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V4h-3.5zM2 5h12v9a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V5z'/>
													</svg>
												</button>
											</div>
										</a>
									</div>
									";
									}else
									echo "
									<center><h3 class='afeter'>Barang Telah Dihapus!</h3></center>									
									<a href='utama.php'>
										<div class='d-grid gap-2 col-6 mx-auto'>
											<button id='tombol' class='btn' type='button' style='margin-bottom: 10px;'>
												<svg xmlns='http://www.w3.org/2000/svg' width='25' height='25' fill='currentColor' class='bi bi-arrow-left' viewBox='0 0 16 16'>
													<path fill-rule='evenodd' d='M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z'/>
												</svg>
											</button>
										</div>
									</a>			
									";											
								?>
								<!-- Akhir Tabel -->					
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- Akhir Utama-->
		<!--Boostrap-->
		<script src="bootstrap/js/bootstrap.bundle.min.js"></script>
		</script>
	</body>
</html>
