<?php
session_start();
if (empty($_SESSION['cart']["arrCart"]))
	$_SESSION['cart']["arrCart"]=array(); 					
?>
<!DOCTYPE html>
<html>
	<head>
		<!--Boostrap-->
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" />
		<!-- Font Awesome -->
		<script src="https://kit.fontawesome.com/ea33b306b1.js" crossorigin="anonymous"></script>
		<!--CSS-->
		<link rel="stylesheet" href="style.css" />
		<title>Toko Mobil Bekas</title>
	</head>
	<body id="bodi">
		<!-- Utama -->
		<section id="utama">
			<div class="container-fluid py-2">
				<div class="row py-2">
					<div class="col-md-12">
						<div id="ber-luar" class="row">
							<div id="ber" class="row">
								<div class="col-md-12 my-2">
									<hr id="baris">
								
									<h6 class="text-center" style="margin-bottom: 0;">SILAHKAN DIPILIH MOBIL BEKAS BERKUALITAS</h6>
									<hr id="baris">
								</div>
								<!-- Mobil -->
								<div id="ca" class="col-md-3 my-3">
									<a class="ag">
										<div id="kart" class="card shadow-sm h-100 rounded-lg">
											<div class="card-img">
												<section class="car">
                                                    <div id="carouselExampleCaptions" class="carousel slide carousel-fade" data-bs-touch="false" data-bs-interval="false">
                                                        <div class="carousel-indicators">
                                                          <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1" style="color: black;"></button>
                                                          <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
                                                          <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
                                                        </div>
                                                        <div class="carousel-inner">
                                                            <div class="carousel-item active">
                                                                <img src="mobilio.jpeg" class="img-thumbnail" style=" object-fit: cover; background-color: #d6d3d3;" alt="Gambar 1">                                                      
															</div>
														</div>
                                                </section>
											</div>
											<div id="cabo" class="card-body">												
												<h6 style="text-align: center;">Mobilio Tahun 2017</h6>						
												<h6 style="text-align: center; margin-bottom: 0;">Rp 200 Juta</h6>
											</div>
											<a href="tambah.php? brg=Mobilio Tahun 2017 &hrg=2000000000 &jml=1">
												<div class="d-grid gap-2 col-6 mx-auto">
													<button id="tombol" class="btn" type="button" style="margin-bottom: 10px;">
														<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-bag-plus-fill" viewBox="0 0 16 16">
															<path fill-rule="evenodd" d="M10.5 3.5a2.5 2.5 0 0 0-5 0V4h5v-.5zm1 0V4H15v10a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V4h3.5v-.5a3.5 3.5 0 1 1 7 0zM8.5 8a.5.5 0 0 0-1 0v1.5H6a.5.5 0 0 0 0 1h1.5V12a.5.5 0 0 0 1 0v-1.5H10a.5.5 0 0 0 0-1H8.5V8z"/>
														</svg>
													</button>
												</div>
											</a>
										</div>
									</a>
								</div>
								<div id="ca" class="col-md-3 my-3">
									<a class="ag">
										<div id="kart" class="card shadow-sm h-100 rounded-lg">
											<div class="card-img">
												<section class="car">
                                                    <div id="carouselExampleCaptions2" class="carousel slide carousel-fade" data-bs-touch="false" data-bs-interval="false">
                                                        <div class="carousel-indicators">
                                                          <button type="button" data-bs-target="#carouselExampleCaptions2" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                                                          <button type="button" data-bs-target="#carouselExampleCaptions2" data-bs-slide-to="1" aria-label="Slide 2"></button>
                                                        </div>
                                                        <div class="carousel-inner">
                                                            <div class="carousel-item active">
                                                                <img src="ertiga.jpg" class="img-thumbnail" style=" object-fit: cover; background-color: #d6d3d3;" alt="Gambar 1">
                                                            </div>
														</div>
                                                </section>
											</div>
											<div id="cabo" class="card-body">												
												<h6 style="text-align: center;">Ertiga Tahun 2020</h6>						
												<h6 style="text-align: center; margin-bottom: 0;">Rp 170 Juta</h6>
											</div>
											<a href="tambah.php? brg=Ertiga Tahun 2020 &hrg=170000000 &jml=1">
												<div class="d-grid gap-2 col-6 mx-auto">
													<button id="tombol" class="btn" type="button" style="margin-bottom: 10px;">
														<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-bag-plus-fill" viewBox="0 0 16 16">
															<path fill-rule="evenodd" d="M10.5 3.5a2.5 2.5 0 0 0-5 0V4h5v-.5zm1 0V4H15v10a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V4h3.5v-.5a3.5 3.5 0 1 1 7 0zM8.5 8a.5.5 0 0 0-1 0v1.5H6a.5.5 0 0 0 0 1h1.5V12a.5.5 0 0 0 1 0v-1.5H10a.5.5 0 0 0 0-1H8.5V8z"/>
														</svg>
													</button>
												</div>
											</a>
										</div>
									</a>
								</div>
								<div id="ca" class="col-md-3 my-3">
									<a class="ag">
										<div id="kart" class="card shadow-sm h-100 rounded-lg">
											<div class="card-img">
												<img src="avanza.jpg" class="img-thumbnail" style=" object-fit: cover; background-color: #d6d3d3;" alt="Gambar 1">
											</div>
											<div id="cabo" class="card-body">												
												<h6 style="text-align: center;">Avanza Tahun 2018</h6>						
												<h6 style="text-align: center; margin-bottom: 0;">Rp 125 Juta</h6>
											</div>
											<a href="tambah.php? brg=Avanza Tahun 2018 &hrg=125000000 &jml=1">														
												<div class="d-grid gap-2 col-6 mx-auto">
													<button id="tombol" class="btn" type="button" style="margin-bottom: 10px;">
														<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-bag-plus-fill" viewBox="0 0 16 16">
															<path fill-rule="evenodd" d="M10.5 3.5a2.5 2.5 0 0 0-5 0V4h5v-.5zm1 0V4H15v10a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V4h3.5v-.5a3.5 3.5 0 1 1 7 0zM8.5 8a.5.5 0 0 0-1 0v1.5H6a.5.5 0 0 0 0 1h1.5V12a.5.5 0 0 0 1 0v-1.5H10a.5.5 0 0 0 0-1H8.5V8z"/>
														</svg>
													</button>
												</div>
											</a>
										</div>
									</a>
								</div>
								<div id="ca" class="col-md-3 my-3">
									<a class="ag">
										<div id="kart" class="card shadow-sm h-100 rounded-lg">
											<div class="card-img">
												<div class="card-img">
													<img src="alphard.jpg" class="img-thumbnail" style=" object-fit: cover; background-color: #d6d3d3;" alt="Gambar 1">
												</div>
											</div>
											<div id="cabo" class="card-body">												
												<h6 style="text-align: center;">Alphard Tahun 2010</h6>						
												<h6 style="text-align: center; margin-bottom: 0;">Rp 688 Juta</h6>
											</div>
											<a href="tambah.php? brg=Alphard Tahun 2010 &hrg=685000000 &jml=1">														
												<div class="d-grid gap-2 col-6 mx-auto">
													<button id="tombol" class="btn" type="button" style="margin-bottom: 10px;">
														<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-bag-plus-fill" viewBox="0 0 16 16">
															<path fill-rule="evenodd" d="M10.5 3.5a2.5 2.5 0 0 0-5 0V4h5v-.5zm1 0V4H15v10a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V4h3.5v-.5a3.5 3.5 0 1 1 7 0zM8.5 8a.5.5 0 0 0-1 0v1.5H6a.5.5 0 0 0 0 1h1.5V12a.5.5 0 0 0 1 0v-1.5H10a.5.5 0 0 0 0-1H8.5V8z"/>
														</svg>
													</button>
												</div>
											</a>
										</div>
									</a>
								</div>
								<!-- Akhir mobil --->
								<div id="ca" class="col-md-3 my-3">
									<a class="ag">
										<div id="kart" class="card shadow-sm h-100 rounded-lg">
											<div class="card-img">
												<section class="car">
                                                    <div id="carouselExampleCaptions3" class="carousel slide carousel-fade" data-bs-touch="false" data-bs-interval="false">
                                                        <div class="carousel-indicators">
                                                          <button type="button" data-bs-target="#carouselExampleCaptions3" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                                                          <button type="button" data-bs-target="#carouselExampleCaptions3" data-bs-slide-to="1" aria-label="Slide 2"></button>
                                                        </div>
                                                        <div class="carousel-inner">
                                                            <div class="carousel-item active">
                                                                <img src="agya.jpg" class="img-thumbnail" style=" object-fit: cover; background-color: #d6d3d3;" alt="Gambar 1">
                                                            </div>   
														</div>
                                                </section>
											</div>
											<div id="cabo" class="card-body">												
												<h6 style="text-align: center;">Agya Tahun 2011</h6>						
												<h6 style="text-align: center; margin-bottom: 0;">Rp 95 Juta</h6>
											</div>
											<a href="tambah.php? brg=Agya Tahun 2011 &hrg=95000000 &jml=1">														
												<div class="d-grid gap-2 col-6 mx-auto">
													<button id="tombol" class="btn" type="button" style="margin-bottom: 10px;">
														<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-bag-plus-fill" viewBox="0 0 16 16">
															<path fill-rule="evenodd" d="M10.5 3.5a2.5 2.5 0 0 0-5 0V4h5v-.5zm1 0V4H15v10a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V4h3.5v-.5a3.5 3.5 0 1 1 7 0zM8.5 8a.5.5 0 0 0-1 0v1.5H6a.5.5 0 0 0 0 1h1.5V12a.5.5 0 0 0 1 0v-1.5H10a.5.5 0 0 0 0-1H8.5V8z"/>
														</svg>
													</button>
												</div>
											</a>
										</div>
									</a>
								</div>
								<div id="ca" class="col-md-3 my-3">
									<a class="ag" >
										<div id="kart" class="card shadow-sm h-100 rounded-lg">
											<div class="card-img">
												<section class="car">
                                                    <div id="carouselExampleCaptions4" class="carousel slide carousel-fade" data-bs-touch="false" data-bs-interval="false">
                                                        <div class="carousel-indicators">
                                                          <button type="button" data-bs-target="#carouselExampleCaptions4" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1" style="color: black;"></button>
                                                          <button type="button" data-bs-target="#carouselExampleCaptions4" data-bs-slide-to="1" aria-label="Slide 2"></button>
                                                          <button type="button" data-bs-target="#carouselExampleCaptions4" data-bs-slide-to="2" aria-label="Slide 3"></button>
                                                        </div>
                                                        <div class="carousel-inner">
                                                            <div class="carousel-item active">
                                                                <img src="calya.jpg" class="img-thumbnail" style=" object-fit: cover; background-color: #d6d3d3;" alt="Gambar 1">
                                                            </div>
                                                        </div>
                                                </section>
											</div>
											<div id="cabo" class="card-body">												
												<h6 style="text-align: center;">Calya Tahun 2015</h6>						
												<h6 style="text-align: center; margin-bottom: 0;">Rp 150 Juta</h6>
											</div>
											<a href="tambah.php? brg=Calya &hrg=150000000 &jml=1">														
												<div class="d-grid gap-2 col-6 mx-auto">
													<button id="tombol" class="btn" type="button" style="margin-bottom: 10px;">
														<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-bag-plus-fill" viewBox="0 0 16 16">
															<path fill-rule="evenodd" d="M10.5 3.5a2.5 2.5 0 0 0-5 0V4h5v-.5zm1 0V4H15v10a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V4h3.5v-.5a3.5 3.5 0 1 1 7 0zM8.5 8a.5.5 0 0 0-1 0v1.5H6a.5.5 0 0 0 0 1h1.5V12a.5.5 0 0 0 1 0v-1.5H10a.5.5 0 0 0 0-1H8.5V8z"/>
														</svg>
													</button>
												</div>
											</a>
										</div>
									</a>
								</div>
								<div id="ca" class="col-md-3 my-3">
									<a class="ag">
										<div id="kart" class="card shadow-sm h-100 rounded-lg">
											<div class="card-img">
												<section class="car">
                                                    <div id="carouselExampleCaptions5" class="carousel slide carousel-fade" data-bs-touch="false" data-bs-interval="false">
                                                        <div class="carousel-indicators">
                                                          <button type="button" data-bs-target="#carouselExampleCaptions5" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                                                          <button type="button" data-bs-target="#carouselExampleCaptions5" data-bs-slide-to="1" aria-label="Slide 2"></button>
                                                        </div>
                                                        <div class="carousel-inner">
                                                            <div class="carousel-item active">
                                                                <img src="inova.jpg" class="img-thumbnail" style=" object-fit: cover; background-color: #d6d3d3;" alt="Gambar 1">
                                                            </div>
                                                        </div>
                                                </section>
											</div>
											<div id="cabo" class="card-body">												
												<h6 style="text-align: center;">Inova Tahun 2012</h6>						
												<h6 style="text-align: center; margin-bottom: 0;">Rp 140 Juta</h6>
											</div>
											<a href="tambah.php? brg=Inova Tahun 2012 &hrg=140000000 &jml=1">														
												<div class="d-grid gap-2 col-6 mx-auto">
													<button id="tombol" class="btn" type="button" style="margin-bottom: 10px;">
														<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-bag-plus-fill" viewBox="0 0 16 16">
															<path fill-rule="evenodd" d="M10.5 3.5a2.5 2.5 0 0 0-5 0V4h5v-.5zm1 0V4H15v10a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V4h3.5v-.5a3.5 3.5 0 1 1 7 0zM8.5 8a.5.5 0 0 0-1 0v1.5H6a.5.5 0 0 0 0 1h1.5V12a.5.5 0 0 0 1 0v-1.5H10a.5.5 0 0 0 0-1H8.5V8z"/>
														</svg>
													</button>
												</div>
											</a>
										</div>
									</a>
								</div>
								<div id="ca" class="col-md-3 my-3">
									<a class="ag">
										<div id="kart" class="card shadow-sm h-100 rounded-lg">
											<div class="card-img">
												<div class="card-img">
													<img src="rush.jpg" class="img-thumbnail" style=" object-fit: cover; background-color: #d6d3d3;" alt="Gambar 1">
												</div>
											</div>
											<div id="cabo" class="card-body">												
												<h6 style="text-align: center;">Rush Tahun 2020 TRD Sportivo</h6>						
												<h6 style="text-align: center; margin-bottom: 0;">Rp 250 Juta</h6>
											</div>
											<a href="tambah.php? brg=Rush Tahun 2020 TRD Sportivo &hrg=250000000 &jml=1">														
												<div class="d-grid gap-2 col-6 mx-auto">
													<button id="tombol" class="btn" type="button" style="margin-bottom: 10px;">
														<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-bag-plus-fill" viewBox="0 0 16 16">
															<path fill-rule="evenodd" d="M10.5 3.5a2.5 2.5 0 0 0-5 0V4h5v-.5zm1 0V4H15v10a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V4h3.5v-.5a3.5 3.5 0 1 1 7 0zM8.5 8a.5.5 0 0 0-1 0v1.5H6a.5.5 0 0 0 0 1h1.5V12a.5.5 0 0 0 1 0v-1.5H10a.5.5 0 0 0 0-1H8.5V8z"/>
														</svg>
													</button>
												</div>
											</a>
										</div>
									</a>
								</div>
								<!-- Akhir Bunga -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- Akhir Utama-->
		<!-- footer -->
			<footer id="foot" style="background-color: #4d4d4d" class="text-white text-center shadow-lg">
				<p>UTS Pemrograman Web Lanjut Sangaji Ramdhani Arya Wirayuda</p>
			</footer>
		<!-- Akhir footer -->
		<!--Boostrap-->
		<script src="bootstrap/js/bootstrap.bundle.min.js"></script>
		</script>
	</body>
</html>
